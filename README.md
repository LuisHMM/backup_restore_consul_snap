# Consul OSS snapshot backup and restore

This repository depends on this:
[Provider Agent role snap](https://gitlab.com/vhgodinez95/provider_agent_role_snap)

## Backup

### Approach

1. Snapshots of consul are taken every 2 minutes.
2. Snapshots are stored using AWS s3 buckets.
3. Backup it's designed as  an automated cron job and a.
 manual procedure for restore.

This is achieved by [Provider Agent role snap](https://gitlab.com/vhgodinez95/provider_agent_role_snap) , that runs a cronjob for `ansible-playbook backup.yml -K`.

## Restore

### Usage

For running playbook:

1. Log into the server.
1. On `backup_restore_consul_snap`, run: `ansible-playbook restore.yml`.
1. Press enter or introduce `yes` to restore the latest snapshot.

    ```sh
    You want to restore the last backup? [yes]: yes
    ```

    > End first case.

1. If you don't want to restore the latest snapshot (introducing `no`).
    1. You must select from a list of occurrences. Select and copy the full name, e.g. `snapshot_2020-05-20T18:56:03Z.tgz`

        ```sh
        TASK [restore : debug] *******************************************************************************
        ok: [localhost] => {
            "msg": [
                "Available snapshots",
                [
                    "snapshot_2020-05-20T18:00:03Z.tgz",
                    "snapshot_2020-05-20T18:02:02Z.tgz",
                    "snapshot_2020-05-20T18:04:03Z.tgz",
                    "snapshot_2020-05-20T18:06:02Z.tgz",
                    "snapshot_2020-05-20T18:08:02Z.tgz",
                    "snapshot_2020-05-20T18:10:03Z.tgz",
                    "snapshot_2020-05-20T18:12:02Z.tgz",
                    "snapshot_2020-05-20T18:14:02Z.tgz",
                    "snapshot_2020-05-20T18:16:03Z.tgz",
                    "snapshot_2020-05-20T18:18:02Z.tgz",
                    "snapshot_2020-05-20T18:20:03Z.tgz",
                    "snapshot_2020-05-20T18:22:02Z.tgz",
                    "snapshot_2020-05-20T18:24:03Z.tgz",
                    "snapshot_2020-05-20T18:26:03Z.tgz",
                    "snapshot_2020-05-20T18:28:02Z.tgz",
                    "snapshot_2020-05-20T18:30:03Z.tgz",
                    "snapshot_2020-05-20T18:32:03Z.tgz",
                    "snapshot_2020-05-20T18:34:02Z.tgz",
                    "snapshot_2020-05-20T18:36:03Z.tgz",
                    "snapshot_2020-05-20T18:38:02Z.tgz",
                    "snapshot_2020-05-20T18:40:03Z.tgz",
                    "snapshot_2020-05-20T18:42:02Z.tgz",
                    "snapshot_2020-05-20T18:44:02Z.tgz",
                    "snapshot_2020-05-20T18:46:03Z.tgz",
                    "snapshot_2020-05-20T18:48:05Z.tgz",
                    "snapshot_2020-05-20T18:50:02Z.tgz",
                    "snapshot_2020-05-20T18:52:03Z.tgz",
                    "snapshot_2020-05-20T18:54:02Z.tgz",
                 ==>"snapshot_2020-05-20T18:56:03Z.tgz",<==
                    "snapshot_2020-05-20T18:58:03Z.tgz",
                    "snapshot_2020-05-20T19:00:03Z.tgz"

                ]
            ]
        }
        ```

    1. Paste the full name in the next prompt.

        ```sh
        TASK [restore : pause] *******************************************************************************
        [restore : pause]
        What snapshot do you want to restore?:

        snapshot_2020-05-20T18:56:03Z.tgz
        ```

    > End second case.

---

New feature that filter by date [in this branch](https://gitlab.com/vhgodinez95/backup_restore_consul_snap/-/tree/filter-by-date).
